  _[TOC]_


## PCB parts 
- Raspberry Pi 3 model B+
- 9-DOF IMU breakout (FXOS8700 + FXAS21002C, datasheet is [here](https://www.adafruit.com/product/3463))
- BME280 breakout, a temperature, humidity and pressure sensor ([BME280 chip datasheet](https://cdn-shop.adafruit.com/product-files/2652/2652.pdf))
- Arduino Nano for the servo control (datasheet [here](https://www.arduino.cc/en/uploads/Main/ArduinoNanoManual23.pdf))
- GPS module Neo-7M ([GPS Neo-7M chip datasheet](https://cdn-shop.adafruit.com/product-files/2652/2652.pdf)
- XBee S2C pro (using SPI to communicate with the R-Pi, you can find the datasheet [here](https://www.digi.com/resources/documentation/digidocs/pdfs/90002002.pdf))
 
 ## PCB description
 Sky runner PCB operates all the breakouts needed for the project payload. The breakouts are connected with the R-Pi through this 4-layer circuit board, transfering the data we need.
  
The R-Pi, using SPI Communication Protocol, is connected with the Arduino Nano which is able to move 4 micro servos FS90 through PWM pins and I2C protocol. A Voltage level shifter is also used in between the above two components. 

For the power system, two converters (buck and boost) are provided. We select only one, depending on the voltage supply. 

The 9-axis IMU & the BME280 both use I2C. We can select if it is the R-Pi or the Arduino Nano that controls them. 

<img src="/images/PCBnew.png" alt="Sky-runner PCB" title="Image from PCBnew KiCad" width = "400" height="380">

## Inputs - Outputs
**Signal Inputs**
- Battery input, with supply of 4.2-2.8V (nom. 3.7V for 1s Li-Ion) if Boost converter is selected, or 8.4-5.6V (nom. 7.4V for 1s Li-Ion) if Buck converter is selected.

**Outputs**
- 21 GPIO pins used from the R-Pi, 4 of these support PWM for the servo control.
-  9 GPIO pins used from the Arduino Nano, 4 of them use PWM for the servos.
- 01x03 vertical JST-XH (PWM-5V-GND) for the board-servo connection.

**Selectors**
- 2 Connectors 01x03 & 2 shunt jumpers, that select the R-Pi or the Arduino Nano for the control of the I2C bus.
- 4 solder jumpers to select if the R-Pi or the Arduino Nano is the servo controller.

**Interboard communications & interfaces**
- I2C protocol (SCL-SDA-Vinput-GND). R-Pi or Arduino Nano will be able to communicate with the servos, IMU, XBee & BME280 through this protocol. We can select if the R-Pi or the Arduino Nano will be the controller. In case we prefer Arduino, using a logic level shifter, a high-to-low voltage transition is provided for the Arduino to work. The edge rate accelerator that the specific logic shifter supports, facilitates the fast ramping of high-to-low transition of voltage.
- SPI protocol (MISO-MOSI-SCK-Vinput-GND). Arduino Nano communicates with the R-Pi using SPI.
- UART protocol(TX-RX-Vinput-GND). The GPS Neo-7M which is controlled by the R-Pi.

<img src="/images/PCBnew_3D.png" alt="Sky-runner PCB" title="Image from PCBnew 3D model" width="800" height="440">

For more images click [here](https://gitlab.com/librespacefoundation/cronos-rocket/sky-runner-payload/sky-runner-hw/-/tree/main/images)
or git-clone the KiCad project [here](https://gitlab.com/librespacefoundation/cronos-rocket/sky-runner-payload/sky-runner-hw/-/tree/main/PCB)
